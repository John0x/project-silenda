﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Encryptions;

namespace Core
{
    /// <summary>
    /// Cryptographics class.
    /// </summary>
    public class Crypto
    {
        private readonly IEncryption _encryption;

        public Crypto(IEncryption encryption)
        {
            if (encryption == null) throw new ArgumentNullException("encryption");

            _encryption = encryption;
        }

        public string Encrypt(string text, string password)
        {
            if (text == null) throw new ArgumentNullException("text");
            if (password == null) throw new ArgumentNullException("password");

            return _encryption.Encrypt(text, password);
        }

        public void EncryptFile(string pathIn, string pathOut, string password)
        {
            if (pathIn == null) throw new ArgumentNullException("pathIn");
            if (pathOut == null) throw new ArgumentNullException("pathOut");
            if (password == null) throw new ArgumentNullException("password");

            _encryption.EncryptFile(pathIn, pathOut, password);
        }

        public string Decrypt(string text, string password)
        {
            if (text == null) throw new ArgumentNullException("text");
            if (password == null) throw new ArgumentNullException("password");

            return _encryption.Decrypt(text, password);
        }

        public void DecryptFile(string pathIn, string pathOut, string password)
        {
            if (pathIn == null) throw new ArgumentNullException("pathIn");
            if (pathOut == null) throw new ArgumentNullException("pathOut");
            if (password == null) throw new ArgumentNullException("password");

            _encryption.DecryptFile(pathIn, pathOut, password);
        }
    }
}