﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Core.Encryptions
{
    public interface IEncryption
    {
        string Encrypt(string text, string password);
        void EncryptFile(string fileIn, string fileOut, string password);

        string Decrypt(string text, string password);
        void DecryptFile(string fileIn, string fileOut, string password);
    }
}
