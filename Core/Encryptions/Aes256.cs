﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Core.Encryptions
{
    /// <summary>
    ///     The implementation of the AES 256 encryption.
    /// </summary>
    public class Aes256 : IEncryption
    {
        #region Fields

        private readonly byte[] _salt =
        {
            0x62, 0x12, 0x22, 0x82, 0x1e, 0x2a,
            0x7d, 0x61, 0x25, 0x77, 0x14, 0x24, 0x15
        };

        #endregion

        #region Methods

        #region Public

        /// <summary>
        ///     Encrypts the given text with the given password using the AES 256 Encryption.
        /// </summary>
        /// <param name="text">The Text to encrypt.</param>
        /// <param name="password">The password to encrypt the text with.</param>
        /// <returns>Returns the encrypted bytes as a Base64 String.</returns>
        public string Encrypt(string text, string password)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(text);

            byte[] encryptedData;
            using (var deriveBytes = new Rfc2898DeriveBytes(password, _salt))
            {
                encryptedData = Encrypt(clearBytes, deriveBytes.GetBytes(32), deriveBytes.GetBytes(16));
            }

            return Convert.ToBase64String(encryptedData);
        }

        /// <summary>
        ///     Encrypts the content of the given file with the given password using the AES 256 Encryption.
        /// </summary>
        /// <param name="fileIn">The path of the input file.</param>
        /// <param name="fileOut">The path of the output file.</param>
        /// <param name="password">The password to encrypt the file with.</param>
        public void EncryptFile(string fileIn, string fileOut, string password)
        {
            using (var fsIn = new FileStream(fileIn, FileMode.Open, FileAccess.Read))
            {
                using (var fsOut = new FileStream(fileOut, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    using (var alg = Rijndael.Create())
                    {
                        using (var deriveBytes = new Rfc2898DeriveBytes(password, _salt))
                        {
                            alg.Key = deriveBytes.GetBytes(32);
                            alg.IV = deriveBytes.GetBytes(16);
                        }

                        using (var cs = new CryptoStream(fsOut, alg.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            const int bufferLen = 4096;
                            var buffer = new byte[bufferLen];
                            int bytesRead;

                            do
                            {
                                bytesRead = fsIn.Read(buffer, 0, bufferLen);

                                cs.Write(buffer, 0, bytesRead);
                            } while (bytesRead != 0);
                        }
                    }
                }
            }
        }

        /// <summary>
        ///     Decrypts the given text with the given password using the AES 256 Encryption.
        /// </summary>
        /// <param name="text">The text to decrypt.</param>
        /// <param name="password">The password the text was encrypted with.</param>
        /// <returns>Returns the decrypted text.</returns>
        public string Decrypt(string text, string password)
        {
            byte[] cipherBytes = Convert.FromBase64String(text);

            byte[] decryptedData;
            using (var deriveBytes = new Rfc2898DeriveBytes(password, _salt))
            {
                decryptedData = Decrypt(cipherBytes, deriveBytes.GetBytes(32), deriveBytes.GetBytes(16));
            }

            return Encoding.Unicode.GetString(decryptedData);
        }

        /// <summary>
        ///     Decrypts the content of the given file with the given password using the AES 256 Encryption.
        /// </summary>
        /// <param name="fileIn">The path of the input file.</param>
        /// <param name="fileOut">The path of the output file.</param>
        /// <param name="password">The password the file was encrypted with.</param>
        public void DecryptFile(string fileIn, string fileOut, string password)
        {
            using (var fsIn = new FileStream(fileIn, FileMode.Open, FileAccess.Read))
            {
                using (var fsOut = new FileStream(fileOut, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    using (Rijndael alg = Rijndael.Create())
                    {
                        using (var deriveBytes = new Rfc2898DeriveBytes(password, _salt))
                        {
                            alg.Key = deriveBytes.GetBytes(32);
                            alg.IV = deriveBytes.GetBytes(16);
                        }


                        using (var cs = new CryptoStream(fsOut, alg.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            const int bufferLen = 4096;
                            var buffer = new byte[bufferLen];
                            int bytesRead;

                            do
                            {
                                bytesRead = fsIn.Read(buffer, 0, bufferLen);

                                cs.Write(buffer, 0, bytesRead);
                            } while (bytesRead != 0);
                        }
                    }
                }
            }
        }

        #endregion

        #region private

        /// <summary>
        ///     Encrypts the given data.
        /// </summary>
        /// <param name="data">The data to be encrypted.</param>
        /// <param name="key">The key to encrypt the data with.</param>
        /// <param name="iv">The initialization vector.</param>
        /// <returns>Returns the encrypted data.</returns>
        private byte[] Encrypt(byte[] data, byte[] key, byte[] iv)
        {
            byte[] encryptedData;
            using (var ms = new MemoryStream())
            {
                using (var alg = Rijndael.Create())
                {
                    alg.Key = key;
                    alg.IV = iv;

                    using (var cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(data, 0, data.Length);
                        cs.Close();
                    }
                    encryptedData = ms.ToArray();
                }
            }

            return encryptedData;
        }

        /// <summary>
        ///     Decryptes the given data
        /// </summary>
        /// <param name="data">The data to be decrypted.</param>
        /// <param name="key">The key to decrypt the data with.</param>
        /// <param name="iv">The initialization vector.</param>
        /// <returns>Returns the decrypted data.</returns>
        private byte[] Decrypt(byte[] data, byte[] key, byte[] iv)
        {
            byte[] decryptedData;
            using (var ms = new MemoryStream())
            {
                using (var alg = Rijndael.Create())
                {
                    alg.Key = key;
                    alg.IV = iv;

                    using (var cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(data, 0, data.Length);

                        cs.Close();
                    }
                    decryptedData = ms.ToArray();
                }
            }

            return decryptedData;
        }

        #endregion

        #endregion
    }
}