﻿using System.Windows;
using Caliburn.Micro;
using Project_Silenda.ViewModels;

namespace Project_Silenda
{
    public class AppBootstrapper : BootstrapperBase
    {
        public AppBootstrapper()
        {
            Start();
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<AppViewModel>();
        }
    }
}
