﻿using System;
using Core;
using Core.Encryptions;

namespace TestConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Silenda - TestApp\n");
            Console.WriteLine("Menu:");
            Console.WriteLine("1: Encrypt Text");
            Console.WriteLine("2: Decrypt Text");
            Console.WriteLine("E: Exit\n");

            var crypter = new Crypto(new Aes256());
            crypter.Encrypt("Hello World", "secret");

            string text = "";
            string password = "";
            string crypted = "";
            string decrypted = "";

            while (true)
            {
                ConsoleKeyInfo input = Console.ReadKey(true);
                switch (input.Key)
                {
                    case ConsoleKey.D1:
                        // Input
                        Console.WriteLine("Please enter the text to encrypt");
                        text = Console.ReadLine();
                        Console.WriteLine("Please enter the password");
                        password = Console.ReadLine();

                        // Processing
                        crypted = crypter.Encrypt(text, password);

                        Console.WriteLine("\nOUTPUT:");
                        Console.WriteLine(crypted);

                        break;
                    case ConsoleKey.D2:
                        // Input
                        Console.WriteLine("Please enter the text to decrypt");
                        text = Console.ReadLine();
                        Console.WriteLine("Please enter the password");
                        password = Console.ReadLine();


                        // Processing
                        try
                        {
                            decrypted = crypter.Decrypt(text, password);
                        }
                        catch (Exception)
                        {
                            Console.WriteLine("Text or password is not correct!");
                            break;
                        }

                        Console.WriteLine("\nOUTPUT:");
                        Console.WriteLine(decrypted);

                        break;

                    case ConsoleKey.E:
                        Console.WriteLine("Exiting now");
                        return;
                        break;
                }
            }
        }
    }
}